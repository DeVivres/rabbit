﻿using System;
using ServiceCommunication.RabbitMQWrapper.Library;

namespace Ponger
{
    class Ponger
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ponger");

            Rabbit rabbit = new Rabbit();

            rabbit.ListenQueue("Ping");

            Console.ReadLine();
        }
    }
}
