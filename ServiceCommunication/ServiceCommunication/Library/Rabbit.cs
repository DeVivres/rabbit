﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ServiceCommunication.RabbitMQWrapper.Library
{
	public class Rabbit
	{
		private IConnectionFactory _factory;
		private IConnection _connection; 
		private IModel _channel;
		private EventingBasicConsumer _consumer;


		public bool ListenQueue(string queueName)
		{
			_factory = new ConnectionFactory() { HostName = "localhost" };
			_connection = _factory.CreateConnection();
			_channel = _connection.CreateModel();

			_channel.ExchangeDeclare("TestExchange", ExchangeType.Direct);
			_channel.QueueDeclare($"{queueName}Queue", true, false, false);
			_channel.QueueBind($"{queueName}Queue", "TestExchange", $"{queueName}Key");

			Console.WriteLine("Waiting for Message");

			_consumer = new EventingBasicConsumer(_channel);
			_consumer.Received += (object messageSender, BasicDeliverEventArgs args) =>
			{
				var body = args.Body.ToArray();
				var message = Encoding.UTF8.GetString(body);
				Console.WriteLine($"\nReceived\n{message} ----- {DateTime.Now}");


				_channel.BasicAck(args.DeliveryTag, false);

				Thread.Sleep(2500);
				string answer = message.Contains("Ping") ? "Pong" : "Ping";
				SendMessageToQueue(answer, answer);
			};
			_channel.BasicConsume($"{queueName}Queue", false, _consumer);

			return true;
		}	

		public bool SendMessageToQueue(string queueName, string message)
		{
			_factory = new ConnectionFactory() { HostName = "localhost" };
			_connection = _factory.CreateConnection();
			_connection.CreateModel();
		
			_channel.ExchangeDeclare("TestExchange", ExchangeType.Direct);

			var body = Encoding.UTF8.GetBytes(message);

			_channel.BasicPublish( "TestExchange", $"{queueName}Key", null, body);
			Console.WriteLine($"\nSent\n{message} ----- {DateTime.Now}");


			return true;
			
		}
	}
}
