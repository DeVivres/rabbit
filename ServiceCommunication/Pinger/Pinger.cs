﻿using System;
using ServiceCommunication.RabbitMQWrapper.Library;


namespace Pinger
{
	class Pinger
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Pinger");

			Rabbit rabbit = new Rabbit();

			rabbit.ListenQueue("Pong");

			rabbit.SendMessageToQueue("Ping", "Ping");

			Console.ReadLine();
		}
	}
}
